#pragma once

#include <CoreMinimal.h>
#include <Kismet/BlueprintFunctionLibrary.h>
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
    MS_Walk UMETA(DisplayName = "Walk movement state"),
    MS_Run UMETA(DisplayName = "Run movement state"),
    MS_Sneak UMETA(DisplayName = "Sneak movement state")
};

USTRUCT(BlueprintType)
struct FBaseSpeedInfo
{
    GENERATED_BODY()
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float Snake = 100.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float Walk = 400.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float Run = 700.0f;
};
