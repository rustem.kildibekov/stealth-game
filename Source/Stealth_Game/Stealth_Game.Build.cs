// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Stealth_Game : ModuleRules
{
	public Stealth_Game(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "AIModule" });
	}
}
