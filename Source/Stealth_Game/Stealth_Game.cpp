// Copyright Epic Games, Inc. All Rights Reserved.

#include "Stealth_Game.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Stealth_Game, "Stealth_Game" );
 