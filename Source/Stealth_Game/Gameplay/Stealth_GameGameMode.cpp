// Copyright Epic Games, Inc. All Rights Reserved.

#include "Stealth_GameGameMode.h"
#include "../Character/Stealth_GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AStealth_GameGameMode::AStealth_GameGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Game/BP_PlayerCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
