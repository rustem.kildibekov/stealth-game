// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Stealth_GameGameMode.generated.h"

UCLASS(minimalapi)
class AStealth_GameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AStealth_GameGameMode();
};



