// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Tools/Types.h"
#include "Stealth_GameCharacter.generated.h"

UCLASS(config=Game)
class AStealth_GameCharacter : public ACharacter
{
	GENERATED_BODY()



public:
	AStealth_GameCharacter();

	virtual void Tick(float DeltaSeconds) override;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	void MoveForward(float Value);
	void MoveRight(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

private:
	float MovementSpeed = 0;
	float MovementAcceleration = 0;
	float TurnRate = 0;
	float LookUpRate = 0;
	EMovementState MovementState = EMovementState::MS_Walk;

	void MovementTick(const float& DeltaSeconds);
	void TurnTick(const float& DeltaSeconds);
	void ChangeState(const EMovementState& State);

protected:
	float MovementSpeedKoef = 1;
	float MovementAccelerationKoef = 1;
	float TurnRateKoef = 1;
	float LookUpRateKoef = 1;

};

