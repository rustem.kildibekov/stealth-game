// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Stealth_GameCharacter.h"
#include "EnemyCharacter.generated.h"

/**
 * 
 */
UCLASS()
class STEALTH_GAME_API AEnemyCharacter : public AStealth_GameCharacter
{
	GENERATED_BODY()

public:
		/** Cone for showing enemy view**/
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
	class UPawnSensingComponent* PawnSensing;





	
public:
	AEnemyCharacter();
};
